FROM nginx
LABEL maintainer="Franz Reinhardt <mail@franz-reinhardt.de>"
COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log
