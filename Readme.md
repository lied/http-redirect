# Simple NGINX service which redirects HTTP traffic to HTTPS
This services was developed because the AWS Elastic Application Loadbalancer does not provide
a simple way to redirect all HTTP traffic to HTTPs.

## How to use it on AWS

ECS Task Definition

    [
      {
        "name": "redirect-to-https",
        "image": "registry.gitlab.com/lied/http-redirect",
        "cpu": 64,
        "memory": 64,
        "essential": true,
        "portMappings": [
          {
            "containerPort": 80
          }
        ]
      }
    ]
